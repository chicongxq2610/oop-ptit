package TinhsoFibonacci;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        long[] f = new long[93];
        int test = input.nextInt();
        f[1] = 1; f[2] = 1;
        for (int i = 3; i <= 92; i++) {
            f[i] = f[i-1] + f[i-2];
        }
        while(test-- > 0) {
            int n = input.nextInt();
            System.out.println(f[n]);
        }
    }
}
