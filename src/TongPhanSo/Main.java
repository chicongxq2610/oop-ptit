package TongPhanSo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        long numerator1 = input.nextLong();
        long denominator1 = input.nextLong();
        long numerator2 = input.nextLong();
        long denominator2 = input.nextLong();
        Fraction fraction1 = new Fraction(numerator1, denominator1);
        Fraction fraction2 = new Fraction(numerator2, denominator2);
        Fraction res = Fraction.sum(fraction1, fraction2);
        res.reduce();
        System.out.println(res);
    }
}
