package SoNguyenToTrongFileNhiPhan;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class Main {

    public static boolean isPrime (int num) {
        if (num < 2) return false;
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) return false;
        }
        return true;
    }

    public static void main(String[] args) throws Exception {
        ObjectInputStream obj = new ObjectInputStream(new FileInputStream("SONGUYEN.in"));
        ArrayList<Integer> list = (ArrayList<Integer>) obj.readObject();
        list.sort(null);
        obj.close();
        int i = 0;
        while (i < list.size() - 1) {
            if (isPrime(list.get(i))) {
                int count = 1;
                while (list.get(i).equals(list.get(i+1)) && i < list.size() - 1) {
                    count++;
                    i++;
                }
                System.out.println(list.get(i) + " " + count);
            }
            i++;
        }
    }
}
