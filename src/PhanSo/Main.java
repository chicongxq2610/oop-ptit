package PhanSo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        long numerator = input.nextLong();
        long denominator = input.nextLong();
        Fraction fraction = new Fraction(numerator, denominator);
        fraction.reduce();
        System.out.println(fraction);
    }
}
