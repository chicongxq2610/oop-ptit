package TriangleP;

public class Triangle {
    private Point point1, point2, point3;

    public Triangle(Point point1, Point point2, Point point3) {
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
    }

    public float calcEdge(float x1, float x2, float y1, float y2) {
        return (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public boolean valid() {
        float edge1 = calcEdge(point1.getX(), point2.getX(), point1.getY(), point2.getY());
        float edge2 = calcEdge(point3.getX(), point2.getX(), point3.getY(), point2.getY());
        float edge3 = calcEdge(point3.getX(), point1.getX(), point3.getY(), point1.getY());
        return (edge1 + edge2 > edge3 && edge1 + edge3 > edge2 && edge3 + edge2 > edge1);
    }

    public String getPerimeter() {
        float edge1 = calcEdge(point1.getX(), point2.getX(), point1.getY(), point2.getY());
        float edge2 = calcEdge(point3.getX(), point2.getX(), point3.getY(), point2.getY());
        float edge3 = calcEdge(point3.getX(), point1.getX(), point3.getY(), point1.getY());
        return String.format("%.3f", (edge1 + edge2 + edge3));
    }
}
