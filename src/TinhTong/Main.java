package TinhTong;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(new File("src\\DATA.in"));
        BigInteger sum = new BigInteger("0");
        while (input.hasNext()) {
            if(input.hasNextInt()) sum.add(new BigInteger(input.next()));
        }
        System.out.println(sum);
    }
}
