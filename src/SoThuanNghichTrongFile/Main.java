package SoThuanNghichTrongFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;

public class Main {

    public static boolean check(int num) {
        if (num < 10 || num % 2 == 0) return false;
        int[] digits = new int[20];
        int count = 0;
        while (num > 0) {
            int digit = num % 10;
            if (digit % 2 == 0) return false;
            digits[count++] = digit;
            num /= 10;
        }
        if (count % 2 == 0) return false;
        for (int i = 0; i < count / 2; i++) {
            if (digits[i] != digits[count-i-1]) return false;
        }
        return true;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ObjectInputStream obj1 = new ObjectInputStream(new FileInputStream("DATA1.in"));
        ObjectInputStream obj2 = new ObjectInputStream(new FileInputStream("DATA2.in"));
        ArrayList<Integer> list1 = (ArrayList<Integer>) obj1.readObject();
        ArrayList<Integer> list2 = (ArrayList<Integer>) obj2.readObject();
        obj1.close();
        obj2.close();
        SortedSet<Integer> mySet = new TreeSet<>();
        mySet.addAll(list1);
        int count = 0;
        for (int num: mySet) {
            if (count == 10) break;
            if (check(num) && list2.contains(num)) {
                int count1 = Collections.frequency(list1, num);
                int count2 = Collections.frequency(list2, num);
                System.out.println(num + " " + (count1 + count2));
                count++;
            }
        }
    }
}
