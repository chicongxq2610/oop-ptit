package DanhSachMonHoc;

public class Subject {
    private String id, name;
    private int tc;

    public Subject(String id, String name, int tc) {
        this.id = id;
        this.name = name;
        this.tc = tc;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getTc() {
        return tc;
    }

    @Override
    public String toString() {
        return this.id + " " + this.name + " " + this.tc;
    }
}
