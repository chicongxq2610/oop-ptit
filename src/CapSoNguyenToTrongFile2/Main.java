package CapSoNguyenToTrongFile2;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

public class Main {

    public static boolean isPrime(int num) {
        if (num < 2) return false;
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) return false;
        }
        return true;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ObjectInputStream obj1 = new ObjectInputStream(new FileInputStream("DATA1.in"));
        ObjectInputStream obj2 = new ObjectInputStream(new FileInputStream("DATA2.in"));
        ArrayList<Integer> list1 = (ArrayList<Integer>) obj1.readObject();
        ArrayList<Integer> list2 = (ArrayList<Integer>) obj2.readObject();
        obj1.close();
        obj2.close();
        SortedSet<Integer> sortedSet = new TreeSet<>();
        sortedSet.addAll(list1);
        for (int n : sortedSet) {
            int m = 1000000 - n;
            if (n < m && isPrime(n) && !list2.contains(m) && !list2.contains(n)) {
                if (isPrime(m) && list1.contains(m)) {
                    System.out.println(n + " " + m);
                }
            }
        }
    }
}