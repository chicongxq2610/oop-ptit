package QuayPhai;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int test = input.nextInt();
        while (test-- > 0) {
            int n = input.nextInt();
            long[] arr = new long[n+1];
            long min = 9999999;
            for (int i = 0; i < n; i++) {
                arr[i] = input.nextLong();
                if (min > arr[i]) min = arr[i];
            }
            int index = 0;
            for (int i = 0; i < n; i++) {
                if (arr[i] == min) {
                    index = i;
                    break;
                }
            }
            System.out.println(index);
        }
    }
}
