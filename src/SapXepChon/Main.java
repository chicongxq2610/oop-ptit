package SapXepChon;

import java.util.Scanner;

public class Main {
    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        for (int i = 0; i < n - 1; i++) {
            System.out.printf("Buoc " + (i + 1) + ": ");
            int min = arr[i], index_min = 0;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < min) {
                    min = arr[j];
                    index_min = j;
                }
            }
            if(index_min != 0) swap(arr, i, index_min);
            for(int ele : arr) System.out.printf(ele + " ");
            System.out.println();
        }
    }
}
