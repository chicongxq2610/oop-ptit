package LoaiPhong;

public class LoaiPhong implements Comparable<LoaiPhong> {
    private String sign;
    private String type;
    private int price;
    private float fee;

    public LoaiPhong(String nextLine) {
        String[] data = nextLine.trim().split("\\s+");
        this.sign = data[0];
        this.type = data[1];
        this.price = Integer.parseInt(data[2]);
        this.fee = Float.parseFloat(data[3]);
    }

    @Override
    public int compareTo(LoaiPhong loaiPhong) {
        return type.compareTo(loaiPhong.type);
    }

    @Override
    public String toString() {
        return sign + " " + type + " " + price + " " + fee;
    }

}
