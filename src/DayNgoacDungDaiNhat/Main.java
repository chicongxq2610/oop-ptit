package DayNgoacDungDaiNhat;

import java.util.Objects;
import java.util.Scanner;
import java.util.Stack;

public class Main {

    public static boolean check(String str) {
        Stack<String> stk = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '(') stk.push(String.valueOf(str.charAt(i)));
            else if (!stk.empty()) stk.pop();
            else return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int t = input.nextInt();
        input.nextLine();
        while (t-- > 0) {
            String str = input.nextLine();
            if (check(str)) System.out.println("YES");
            else System.out.println("NO");
        }
    }
}
