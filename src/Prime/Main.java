package Prime;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int test = input.nextInt();
        while (test-- > 0) {
            int num = input.nextInt();
            System.out.println(Number.isPrime(num));
        }
    }
}
