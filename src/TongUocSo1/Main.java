package TongUocSo1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int MAX = 2000001;
        long[] a = new long[MAX+1];
        for (int i = 2; i < MAX; i++) a[i] = 0;
        for (int i = 2; i < MAX; i++) {
            for (int j = 2; j * i <= MAX; j++) {
                if(a[i] == 0) a[i] = i;
                if(a[j] != 0 && j * i <= MAX) a[i * j] = a[i] + a[j];
            }
            if(a[i] == 0) a[i] = i;
        }
        int n = input.nextInt();
        long res = 0;
        int num;
        for (int i = 0; i < n; i++) {
            num = input.nextInt();
            res += a[num];
        }
        System.out.println(res);
    }
}
