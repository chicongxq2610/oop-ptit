package ChuanHoaXauHoTen1;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        String c = sc.nextLine();
        while(test>0){
            String name = sc.nextLine();
            StringTokenizer st = new StringTokenizer(name);
            while (st.hasMoreTokens()) {
                String str = st.nextToken().toLowerCase();
                String cap = str.substring(0, 1).toUpperCase() + str.substring(1);
                System.out.print(cap + " ");
            }
            System.out.print("\n");

            test--;
        }
    }
}
