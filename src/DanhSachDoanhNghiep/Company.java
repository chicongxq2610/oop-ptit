package DanhSachDoanhNghiep;

public class Company {
    private String id, name;
    private int maxStudent;

    public Company(String id, String name, int maxStudent) {
        this.id = id;
        this.name = name;
        this.maxStudent = maxStudent;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + maxStudent;
    }
}
