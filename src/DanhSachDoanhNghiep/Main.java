package DanhSachDoanhNghiep;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(new File("DN.in"));
        int t = input.nextInt();
        input.nextLine();
        ArrayList<Company> companies = new ArrayList<>();
        while (t-- > 0) {
            Company company = new Company(input.nextLine(), input.nextLine(), Integer.parseInt(input.nextLine()));
            companies.add(company);
        }
        Collections.sort(companies, new Comparator<Company>() {
            @Override
            public int compare(Company o1, Company o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        companies.forEach(System.out::println);
    }
}
