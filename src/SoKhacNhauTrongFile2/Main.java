package SoKhacNhauTrongFile2;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        DataInputStream input = new DataInputStream(new FileInputStream("DATA.IN"));
        TreeMap<Integer, Integer> map = new TreeMap<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });
        int t = 100000;
        while(t-- > 0) {
            Integer num = input.readInt();
            if(map.containsKey(num)) {
                map.put(num, map.get(num) + 1);
            }
            else map.put(num, 1);
        }
        show(map);
    }
    public static void show(Map<Integer, Integer> map) {
        Set<Integer> keySet = map.keySet();
        for (Integer key : keySet) {
            System.out.println(key + " " + map.get(key));
        }
    }

}
