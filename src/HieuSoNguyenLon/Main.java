package HieuSoNguyenLon;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0){
            BigInteger num1 = sc.nextBigInteger();
            BigInteger num2 = sc.nextBigInteger();
            int len1 = num1.toString().length();
            int len2 = num2.toString().length();
            num1 = (num1.subtract(num2)).abs();
            String res = num1.toString();
            while (res.length() < len1)
                res = "0" + res;
            while (res.length() < len2)
                res = "0" + res;
            System.out.println(res);
        }
    }
}
