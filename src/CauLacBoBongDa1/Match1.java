package CauLacBoBongDa1;

public class Match1 {
    private String id;
    private long numOfFan;
    private long revenue;
    private String clubName;

    public Match1(String id, long numOfFan) {
        this.id = id;
        this.numOfFan = numOfFan;
    }

    public String getClubId() {
        return id.charAt(1)+ "" + id.charAt(2) ;
    }

    public long getRevenue() {
        return revenue;
    }

    public void calcRevenue(Club club) {
        if (getClubId().equals(club.getId())) {
            revenue =  (club.getTicketPrice() * numOfFan);
            clubName = club.getName();
        }
    }
}
