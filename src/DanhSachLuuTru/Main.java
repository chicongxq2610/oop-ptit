package DanhSachLuuTru;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ParseException {
        ArrayList<Customer> customers = new ArrayList<>();
        Scanner input = new Scanner(new File("KHACH.in"));
        int t = input.nextInt();
        input.nextLine();
        for (int i = 1; i <= t; i++) {
            Customer customer = new Customer(
                    String.format("KH%02d", i) ,
                    input.nextLine(),
                    input.nextLine(),
                    new SimpleDateFormat("dd/MM/yyyy").parse(input.nextLine()),
                    new SimpleDateFormat("dd/MM/yyyy").parse(input.nextLine())
            );
            customers.add(customer);
        }
        customers.sort(null);
        customers.forEach(System.out::println);
    }
}
