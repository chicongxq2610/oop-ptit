package MangDoiXung;

public class IntArray {
    public static String isPalindrome(int[] arr, int n) {
        for (int i = 0; i < n/2; i++) {
            if (arr[i] != arr[n-i-1]) return "NO";
        }
        return "YES";
    }
}
