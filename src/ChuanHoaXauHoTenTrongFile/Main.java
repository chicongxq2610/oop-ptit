package ChuanHoaXauHoTenTrongFile;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(new File("src\\DATA.in"));
        while (input.hasNextLine()) {
            String str = input.nextLine();
            if (str.equals("end")) break;
            str = str.toLowerCase().trim().replaceAll("\\s+", " ");
            String[] tokens = str.split(" ");
            for (int i = 0; i < tokens.length; i++) {
                String res = String.valueOf(tokens[i].charAt(0)).toUpperCase() + tokens[i].substring(1);
                System.out.printf(res + " ");
            }
            System.out.println();
        }
    }
}
