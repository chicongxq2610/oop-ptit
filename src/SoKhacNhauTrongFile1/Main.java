package SoKhacNhauTrongFile1;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(new File("src\\DATA.in"));
        SortedMap<Integer, Integer> map = new TreeMap<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });
        while(input.hasNextInt()) {
            Integer num = input.nextInt();
            if(map.containsKey(num)) {
                map.put(num, map.get(num) + 1);
            }
            else map.put(num, 1);
        }
        show(map);
    }
    public static void show(Map<Integer, Integer> map) {
        Set<Integer> keySet = map.keySet();
        for (Integer key : keySet) {
            System.out.println(key + " " + map.get(key));
        }
    }
}
