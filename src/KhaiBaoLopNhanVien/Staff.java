package KhaiBaoLopNhanVien;

public class Staff {
    public Staff(String name, String gender, String birthday, String address, String taxID, String duntKnow) {
        this.name = name;
        this.gender = gender;
        this.birthday = birthday;
        this.address = address;
        this.taxID = taxID;
        this.duntKnow = duntKnow;
    }

    String name;
    String gender;
    String birthday;
    String address;
    String taxID;
    String duntKnow;

    @Override
    public String toString() {
        return "00001" + " "
                + name + " "
                + gender + " "
                + birthday + " "
                + address + " "
                + taxID + " "
                +duntKnow;
    }
}
