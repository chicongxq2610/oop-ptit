package LietKeTuKhacNhau;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class WordSet {
    private String fileName;

    public WordSet(String fileName) {
        this.fileName = fileName;
    }

    public String listWord() throws FileNotFoundException {
        Scanner input = new Scanner(new File("VANBAN.in"));
        Set<String> set = new HashSet<>();
        while (input.hasNext()) {
            String word = input.next().toLowerCase();
            set.add(word);
        }
        List<String> words = new ArrayList<>(set);
        Collections.sort(words);
        String res = "";
        for (String word : words) {
            res += word;
            res += '\n';
        }
        return res;
    }

    @Override
    public String toString() {
        String res = "";
        try {
            res = listWord();
        }
        catch (FileNotFoundException e) {

         }
        return res;
    }
}
