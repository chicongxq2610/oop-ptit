package Prime;

public class Number {
    public static String isPrime(int num) {
        if (num < 2) return "NO";
        else if (num == 2) return "YES";
        else {
            for (int i = 2; i <= Math.sqrt(num); i++) {
                if (num % i == 0) return "NO";
            }
            return "YES";
        }
    }
}
