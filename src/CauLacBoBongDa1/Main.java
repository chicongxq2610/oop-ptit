package CauLacBoBongDa1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Club> clubList = new ArrayList();
        ArrayList<Match1> matchList = new ArrayList();
        Scanner input = new Scanner(System.in);
        int n1 = input.nextInt();
        while (n1-- > 0) {
            input.nextLine();
            Club club = new Club(input.nextLine(), input.nextLine(), input.nextLong());
            clubList.add(club);
        }
        int n2 = input.nextInt();
        while (n2-- > 0) {
            input.nextLine();
            Match1 match1 = new Match1(input.next(), input.nextLong());
            for (int i = 0; i < clubList.size(); i++) {
                match1.calcRevenue(clubList.get(i));
            }
        }
    }
}
