package DanhSachMonHoc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(new File("MONHOC.in"));
        int t = input.nextInt();
        input.nextLine();
        ArrayList<Subject> list = new ArrayList<>();
        while(t-- > 0) {
            Subject subject = new Subject(input.nextLine(), input.nextLine(), Integer.parseInt(input.nextLine()));
            list.add(subject);
        }
        Collections.sort(list, new Comparator<Subject>() {
            @Override
            public int compare(Subject o1, Subject o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        list.forEach(System.out::println);
    }
}
