package KhaiBaoLopThiSinh;

public class Candidate {
    private String fullName, dob;
    private float mark1, mark2, mark3;

    public Candidate(String fullName, String dob, float mark1, float mark2, float mark3) {
        this.fullName = fullName;
        this.dob = dob;
        this.mark1 = mark1;
        this.mark2 = mark2;
        this.mark3 = mark3;
    }

    @Override
    public String toString() {
        return fullName + ' ' + dob + ' ' + String.format("%.1f", (mark1 + mark2 + mark3));
    }
}
