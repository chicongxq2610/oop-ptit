package UocSoChiaHetCho2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int test = input.nextInt();
        while (test-- > 0) {
            int n = input.nextInt();
            int i, cnt = 0;
            for (i = 1; i <= Math.sqrt(n); i++) {
                if (n % i == 0) {
                    if (i % 2 == 0) cnt++;
                    if ((n / i) % 2 == 0) cnt++;
                }
            }
            i--;
            if ((i * i == n) && (i % 2 == 0)) {
                cnt--;
            }
            System.out.println(cnt);
        }
    }
}
