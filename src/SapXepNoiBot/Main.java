package SapXepNoiBot;

import java.util.Scanner;

public class Main {
    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        boolean haveSwap;
        for (int i = 0; i < n - 1; i++) {
            haveSwap = false;
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j+1]) {
                    swap(arr, j, j + 1);
                    haveSwap = true;
                }
            }
            if(!haveSwap) break;
            System.out.printf("Buoc " + (i + 1) + ": ");
            for(int k : arr) System.out.printf(k + " ");
            System.out.println();
        }
    }
}