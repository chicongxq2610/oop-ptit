package CauLacBoBongDa1;

public class Club {
    private String id, name;
    private long ticketPrice;

    public Club(String id, String name, long ticketPrice) {
        this.id = id;
        this.name = name;
        this.ticketPrice = ticketPrice;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getTicketPrice() {
        return ticketPrice;
    }
}
