package KhaiBaoLopSinhVien;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner input = new Scanner(System.in);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Student student = new Student();
        student.setFullName(input.nextLine());
        student.setIdClass(input.nextLine());
        student.setDob(formatter.format(formatter.parse(input.nextLine())));
        student.setGpa(Float.parseFloat(input.nextLine()));
        System.out.printf(student.toString());
    }
}
