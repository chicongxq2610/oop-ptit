package DanhSachSinhVienTrongFileVanBan;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ParseException {
        Scanner input = new Scanner(new File("SV.in"));
        int t = input.nextInt();
        input.nextLine();
        ArrayList<Student> students = new ArrayList<>();
        while (t-- > 0) {
            Student student = new Student(input.nextLine(), input.nextLine(), input.nextLine(), (float) Double.parseDouble(input.nextLine()));
            students.add(student);
        }
        students.forEach(System.out::println);
    }
}
