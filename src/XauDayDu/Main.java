package XauDayDu;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int test = input.nextInt();
        while(test-- > 0) {
            input.nextLine();
            String s = input.nextLine();
            int k = input.nextInt();
            long uniqueChar =  s.chars().distinct().count();
            System.out.println(26 - uniqueChar <= k ? "YES" : "NO");
        }
    }
}
