package ThuaSoNguyenTo;

import java.util.Scanner;

public class Main {
    static long pow(int P, int K) throws ArithmeticException {
        long res = 1;
        for (int i = 0; i < K; i++) {
            res *= P;
            if (res > 1000000000) throw new ArithmeticException("demo");
        }
        return res;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int K = input.nextInt();
        int P = input.nextInt();
        long res = 1;
        try {
            res = pow(P, K);
        }
        catch (ArithmeticException err) {
            res = 0;
        }


        System.out.println(res);
    }
}
