package KhaiBaoLopSinhVien;

public class Student {
    private String id, fullName, idClass, dob;
    float gpa;

    public Student() {
        this.id = "B20DCCN001";
        this.fullName = "";
        this.idClass = "";
        this.dob = "";
        this.gpa = 0;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setIdClass(String idClass) {
        this.idClass = idClass;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }

    @Override
    public String toString() {
        return  id + ' ' + fullName + ' ' + idClass + ' ' + dob + ' ' + String.format("%.2f", gpa);
    }
}
