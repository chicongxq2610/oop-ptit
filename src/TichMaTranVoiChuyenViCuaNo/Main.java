package TichMaTranVoiChuyenViCuaNo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        int crTest = 1;
        while(test>0){
            int hang = sc.nextInt();
            int cot = sc.nextInt();
            int[][] a = new int[hang+1][cot+1];
            int[][] b = new int[hang+1][cot+1];
            int[][] C = new int[hang+1][cot+1];
            for(int i=0;i<hang;i++){
                for(int j=0;j<cot;j++){
                    a[i][j] = sc.nextInt();
                }
            }

            for(int i=0;i<cot;i++){
                for(int j=0;j<hang;j++){
                    b[i][j] = a[j][i];
                }
            }


            for(int i=0;i<hang;i++)
            {
                for(int j=0;j<hang;j++)
                {
                    for(int k=0;k<cot;k++)
                    {
                        C[i][j]+=a[i][k]*b[k][j];
                    }
                }
            }


            System.out.println("Test " + crTest + ":");
            for(int i=0;i<hang;i++){
                for(int j=0;j<hang;j++){
                    System.out.print(C[i][j] + " ") ;
                }
                System.out.println();
            }

            test--;
            crTest++;

        }
    }
}
