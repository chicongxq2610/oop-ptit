package UocSoChungLonNhatCuaSoNguyenLon;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int test = input.nextInt();
        while (test-- > 0) {
            BigInteger num1 = input.nextBigInteger();
            BigInteger num2 = input.nextBigInteger();
            System.out.println(num1.gcd(num2));
        }
    }
}
