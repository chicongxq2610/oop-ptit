package SoNguyenToLonNhatTrongFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;

public class Main {
    public static boolean isPrime(int num) {
        if (num <= 1) return false;
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) return false;
        }
        return true;
    }

    public static void show(Map<Integer, Integer> map) {
        Iterator<Integer> itr = map.keySet().iterator();
        int cnt = 0;
        while (itr.hasNext()) {
            cnt++;
            int num = itr.next();
            System.out.println(num + " " + map.get(num));
            if (cnt == 10) break;
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileInputStream file = new FileInputStream("DATA.in");
        ObjectInputStream objInput = new ObjectInputStream(file);
        ArrayList<Integer> list = (ArrayList<Integer>) objInput.readObject();
        objInput.close();
        SortedMap<Integer, Integer> map = new TreeMap<>(Collections.reverseOrder());

        for (int i : list) {
            if (isPrime(i)) {
                if (map.containsKey(i)) {
                    map.put(i, map.get(i) + 1);
                }
                else map.put(i, 1);
            }
        }
        show(map);
    }
}
