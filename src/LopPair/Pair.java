package LopPair;

public class Pair <T1, T2>{
    private T1 num1;
    private T2 num2;

    public Pair (T1 num1, T2 num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    private boolean checkPrime(int n) {
        if (n < 2) return false;
        int squareRoot = (int) Math.sqrt(n);
        for (int i = 2; i <= squareRoot; i++) {
            if (n % i == 0) return false;
        }
        return true;
    }

    public boolean isPrime () {
        return checkPrime((Integer) num1) && checkPrime((Integer) num2);
    }

    @Override
    public String toString() {
        return num1 + " " + num2;
    }
}
