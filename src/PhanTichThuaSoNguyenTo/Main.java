package PhanTichThuaSoNguyenTo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int test = input.nextInt();
        int cnt = 1;
        while (test-- > 0) {
            int n = input.nextInt();
            int k = 2, count = 0;
            System.out.printf("Test " + (cnt++) + ": ");
            while (n > 1) {
                while (n % k == 0) {
                    n /= k;
                    count++;
                }
                if (count != 0) System.out.printf(k + "(" + count + ") ");
                count = 0;
                k++;
            }
            System.out.println();
        }
    }
}
