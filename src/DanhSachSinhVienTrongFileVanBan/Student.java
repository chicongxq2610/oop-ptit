package DanhSachSinhVienTrongFileVanBan;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Student {
    private String fullName, id, classId;
    private float gpa;
    private Date dob;
    private static int count = 0;

    public Student(String fullName, String classId, String dob, float gpa) throws ParseException {
        count++;
        this.fullName = fullName;
        this.id = "B20DCCN" + String.format("%03d", count);
        this.classId = classId;
        this.gpa = gpa;
        this.dob = new SimpleDateFormat("dd/MM/yyyy").parse(dob);
    }

    @Override
    public String toString () {
        return id + " " + fullName + " " + classId + " " + new SimpleDateFormat("dd/MM/yyyy").format(dob) + " " + String.format("%.2f", gpa);
    }
}
