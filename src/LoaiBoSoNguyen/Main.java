package LoaiBoSoNguyen;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static boolean isInteger(String str) {
        try {
            int num = Integer.parseInt(str);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(new File("src\\DATA.in"));
        ArrayList<String> words = new ArrayList<>();
        while (input.hasNext()) {
            String str = input.next();
            if (!isInteger(str)) words.add(str);
        }
        words.sort(null);
        for (String num : words) {
            System.out.printf(num + " ");
        }
    }
}
