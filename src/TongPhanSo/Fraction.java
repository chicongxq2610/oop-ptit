package TongPhanSo;

public class Fraction {
    private long numerator, denominator;

    public Fraction(long numerator, long denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public static Fraction sum(Fraction fraction1, Fraction fraction2) {
        long numerator = fraction1.numerator * fraction2.denominator + fraction2.numerator * fraction1.denominator;
        long denominator = fraction1.denominator * fraction2.denominator;
        return new Fraction(numerator, denominator);
    }

    public long gcd(long a, long b){
        while(b != 0){
            long tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

    public void reduce() {
        long gcd = gcd(numerator, denominator);
        numerator /= gcd;
        denominator /= gcd;
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }
}
