package BoiSoNhoNhatCuaNSoNguyenDuongDauTien;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static int MAX = 100;
    static ArrayList<Integer> primes = new ArrayList<Integer>();
    static void sieve() {
        boolean[] isComposite = new boolean[MAX + 1];
        for (int i = 2; i * i <= MAX; i++) {
            if (!isComposite[i]) {
                for (int j = 2; j * i <= MAX; j++)
                    isComposite[i * j] = true;
            }
        }
        for (int i = 2; i <= MAX; i++)
            if (isComposite[i] == false)
                primes.add(i);
    }

    static long LCM(int n)
    {
        long lcm = 1;
        for (int i = 0;
             i < primes.size() && primes.get(i) <= n;
             i++)
        {
            long pp = primes.get(i);
            while (pp * primes.get(i) <= n)
                pp = pp * primes.get(i);

            lcm *= pp;
        }
        return lcm;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int test = input.nextInt();
        sieve();
        while (test-- > 0) {
            int n = input.nextInt();
            System.out.println(LCM(n));
        }
    }
}
