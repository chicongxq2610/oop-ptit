package MangDoiXung;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int test = input.nextInt();
        while (test-- > 0) {
            int n = input.nextInt();
            int[] arr = new int[n+1];
            for (int i = 0; i < n; i++) {
                arr[i] = input.nextInt();
            }
            System.out.println(IntArray.isPalindrome(arr, n));
        }
    }
}
