package MaTranNhiPhan;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int res = 0;
        for (int i = 0; i < n; i++) {
            int sumRow = 0;
            for (int j = 0; j < 3; j++) {
                sumRow += input.nextInt();
            }
            if (sumRow > 1) res++;
        }
        System.out.println(res);
    }
}
