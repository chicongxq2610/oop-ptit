package KhaiBaoLopThiSinh;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner input = new Scanner(System.in);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String fullName = input.nextLine();
        String dob = formatter.format(formatter.parse(input.nextLine()));
        String mark1 = input.nextLine();
        String mark2 = input.nextLine();
        String mark3 = input.nextLine();
        Candidate candidate = new Candidate(fullName, dob, Float.parseFloat(mark1), Float.parseFloat(mark2), Float.parseFloat(mark3));
        System.out.println(candidate.toString());
    }
}
