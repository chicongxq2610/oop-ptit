package HinhSao;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int t = input.nextInt();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < t - 1; i++) {
            int u = input.nextInt();
            int v = input.nextInt();
            if (map.containsKey(u)) {
                map.put(u, map.get(u) + 1);
            }
            else map.put(u, 1);
            if (map.containsKey(v)) {
                map.put(v, map.get(v) + 1);
            }
            else map.put(v, 1);
        }
        for (Integer key : map.keySet()) {
            if (map.get(key) == t - 1) {
                System.out.println("Yes");
                return;
            }
        }
        System.out.println("No");
    }

}
