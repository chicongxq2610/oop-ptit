package BoiSoChung_UocSoChung;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {

    public static long UCLN(long a, long b) {
        if (b == 0) return a;
        return UCLN(b, a % b);
    }

    public static long BCNN(long a, long b) {
        return (a * b) / UCLN(a, b);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int test = input.nextInt();
        while (test-- > 0) {
            int a = input.nextInt();
            int b = input.nextInt();
            System.out.println(BCNN(a, b) + " " +  UCLN(a, b));
        }
    }
}
