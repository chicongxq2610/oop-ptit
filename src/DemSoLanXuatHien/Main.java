package DemSoLanXuatHien;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int test = input.nextInt();
        int cnt = 1;
        while (test-- > 0) {
            int n = input.nextInt();
            int[] arr = new int[n+1];
            List<Integer> clist = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                arr[i] = input.nextInt();
                clist.add(arr[i]);
            }
            System.out.println("Test " + cnt++ + ":");
            for (int i : clist) {
                if (Collections.frequency(clist, i) > 0) {
                    System.out.println(i + " xuat hien " + Collections.frequency(clist, i) + " lan");
                    clist = clist.stream().filter(num -> num != i).collect(Collectors.toList());
                }
            }
        }
    }
}
